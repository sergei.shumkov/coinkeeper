package org.shumkov.coin.keeper.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class CoinkeeperRegistryApplication
{

  public static void main(String[] args)
  {
    SpringApplication.run(CoinkeeperRegistryApplication.class, args);
  }

}
